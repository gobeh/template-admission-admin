package com.example.spmbint.services;

import com.example.spmbint.dto.CountryDto;
import com.example.spmbint.dto.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TestService {
    public List<UserDto> getUserList() {
        List<UserDto> result = new ArrayList<>();

        long i = 1L;
        for (int j = 0; j <= 12; j++) {
            UserDto userDto = new UserDto();
            userDto.setId(i);
            userDto.setName("Orang ke-" + i);
            userDto.setBirthDate(LocalDate.now());
            userDto.setEmail(userDto.getName() + "@tes.com");
            userDto.setCity("Madrid");
            userDto.setCountry("Spain");
            result.add(userDto);
            i++;
        }
        return result;
    }

    public List<UserDto> getUserList(String role) {
        List<UserDto> result = new ArrayList<>();

        long i = 1L;
        for (int j = 0; j <= 12; j++) {
            UserDto userDto = new UserDto();
            userDto.setId(i);
            userDto.setName(role + " ke-" + i);
            userDto.setBirthDate(LocalDate.now());
            userDto.setEmail(userDto.getName() + "@tes.com");
            userDto.setCity("Madrid");
            userDto.setCountry("Spain");
            userDto.setRole(role);
            result.add(userDto);
            i++;
        }
        return result;
    }

    public List<UserDto> getUserListByName(String name, String role) {
        List<UserDto> data = getUserList(role);
        List<UserDto> filteredData;

        filteredData = data.stream()
                .filter(item -> item.getName().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());

        return filteredData;
    }

    public List<UserDto> getUserListById(long id) {
        List<UserDto> data = getUserList();
        List<UserDto> filteredData;

        filteredData = data.stream()
                .filter(item -> item.getId() == id)
                .collect(Collectors.toList());

        return filteredData;
    }

    public List<UserDto> getUserListById(long id, String role) {
        List<UserDto> data = getUserList(role);
        List<UserDto> filteredData;

        filteredData = data.stream()
                .filter(item -> item.getId() == id && item.getRole().equals(role))
                .collect(Collectors.toList());

        return filteredData;
    }

    public List<CountryDto> getCountryList() {
        List<CountryDto> result = new ArrayList<>();

        CountryDto countryDto = new CountryDto(1L, "England");
        result.add(countryDto);
        countryDto = new CountryDto(2L, "France");
        result.add(countryDto);
        countryDto = new CountryDto(3L, "Egypt");
        result.add(countryDto);

        return result;
    }

    public Page<UserDto> getPageFromUserList(Pageable page) {
        List<UserDto> inputList = getUserList();

        Pageable paging = PageRequest.of(page.getPageNumber(), page.getPageSize());
        int start = Math.min((int) paging.getOffset(), inputList.size());
        int end = Math.min((start + paging.getPageSize()), inputList.size());

        return new PageImpl<>(inputList.subList(start, end), paging, inputList.size());
    }

    public Page<UserDto> getPageFromUserList(Pageable page, String role) {
        List<UserDto> inputList = getUserList(role);

        Pageable paging = PageRequest.of(page.getPageNumber(), page.getPageSize());
        int start = Math.min((int) paging.getOffset(), inputList.size());
        int end = Math.min((start + paging.getPageSize()), inputList.size());

        return new PageImpl<>(inputList.subList(start, end), paging, inputList.size());
    }

    public Page<UserDto> getPageFromUserListByUser(Pageable page, long userSearch) {
        List<UserDto> inputList = getUserListById(userSearch);

        Pageable paging = PageRequest.of(page.getPageNumber(), page.getPageSize());
        int start = Math.min((int) paging.getOffset(), inputList.size());
        int end = Math.min((start + paging.getPageSize()), inputList.size());

        return new PageImpl<>(inputList.subList(start, end), paging, inputList.size());
    }

    public Page<UserDto> getPageFromUserListByUser(Pageable page, long userSearch, String role) {
        List<UserDto> inputList = getUserListById(userSearch, role);

        Pageable paging = PageRequest.of(page.getPageNumber(), page.getPageSize());
        int start = Math.min((int) paging.getOffset(), inputList.size());
        int end = Math.min((start + paging.getPageSize()), inputList.size());

        return new PageImpl<>(inputList.subList(start, end), paging, inputList.size());
    }
}

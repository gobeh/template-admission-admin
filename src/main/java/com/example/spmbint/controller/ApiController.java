package com.example.spmbint.controller;

import com.example.spmbint.dto.UserDto;
import com.example.spmbint.services.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private TestService testService;

    @GetMapping("/users")
    public List<UserDto> getAllUsers(@RequestParam(value = "name", required = false) String name,
                                     @RequestParam(value = "role", required = false) String role) {
        log.info("name: {}", name);
        if (name == null) {
            return testService.getUserList(role);
        } else {
            return testService.getUserListByName(name.toLowerCase(), role);
        }
    }

}

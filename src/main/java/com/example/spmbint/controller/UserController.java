package com.example.spmbint.controller;

import com.example.spmbint.dto.UserDto;
import com.example.spmbint.services.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;

@Controller
@RequestMapping(value = "/admin/user")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private TestService testService;

    @GetMapping(value = {"/", "", "/list"})
    public String getList(ModelMap mm, @RequestParam(value = "page", required = false) String page) {

        mm.addAttribute("listCountry", testService.getCountryList());
        mm.addAttribute("page", page);

        return "admin/user/list";
    }

    @GetMapping(value = {"/{tab}"})
    public String tab(ModelMap mm, @PathVariable String tab, @RequestParam(value = "userSearch", required = false) String userSearch, @PageableDefault(size = 10) Pageable page) {
        if (Arrays.asList("admin", "user")
                .contains(tab)) {

            Page<UserDto> result;
            if (userSearch == null)
                result = testService.getPageFromUserList(page, tab);
            else
                result = testService.getPageFromUserListByUser(page, Long.parseLong(userSearch), tab);

            mm.addAttribute("data", result);
            mm.addAttribute("listCountry", testService.getCountryList());
            mm.addAttribute("userSearch", userSearch);

            return "admin/user/_" + tab;
        }

        return "";
    }
}
